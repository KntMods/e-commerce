<?php

namespace App\Controllers;

use App\Models\ShopModel;


class Home extends BaseController
{
    public function index()
    {
        return view('shop/index');
    }
    public function contact()
    {
        return view('shop/contact');
    }
    public function shop()
    {
        $shopModel = new ShopModel();
        $data['products'] = $shopModel->findAll();

        return view('shop/shop', $data);
    }
    public function about()
    {
        return view('shop/about');
    }
    public function detile($id)
    {
        $productModel = new ShopModel();
        // Create an instance of the product model


        // Fetch the product data from the model based on the $id
        $productData = $productModel->find($id);

        // Check if the product data is available
        if ($productData) {
            // Pass the product data to the view
            $data = [
                'images' => $productData['images'],
                'productName' => $productData['name'],
                'productDescription' => $productData['deskripsi'],
                'productPrice' => $productData['price']
            ];

            // Load the view and pass the data
            return view('shop/detailes', $data);
        } else {
            // Handle the case when the product data is not found
            // Redirect or show an error message
        }
    }
}
